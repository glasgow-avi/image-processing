package app;

import gui.Console;
import tools.FileHandler;
import tools.Toolbox;
import utilities.AdvancedColor;
import utilities.Timer;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageProcessor
{
	private static Console window = new Console();

	public static void process(String... args)
	{
		Timer.startTimingEvent();
		BufferedImage image = FileHandler.load(args[0]);
		Timer.stopTimingEvent("File loaded");

		Timer.startTimingEvent();
		Color[][] pixels = Toolbox.getPixels(Toolbox.grayScale(image));
		Timer.stopTimingEvent("Got pixels");

		Timer.startTimingEvent();
		for(int y = 0; y < pixels[0].length; y++)
			for(int x = 0; x < pixels.length; x++)
			{
				AdvancedColor thisPixel = new AdvancedColor(pixels[x][y]);
				if(thisPixel.isApproximately(Color.black, 150))
					pixels[x][y] = Color.black;
				else
					pixels[x][y] = Color.white;
			}
		Timer.stopTimingEvent("Changed pixels");

		Timer.startTimingEvent();
		BufferedImage finalImage = Toolbox.putPixels(pixels);
		Timer.stopTimingEvent("Put pixels");

		Timer.startTimingEvent();
		FileHandler.write(finalImage, args[1]);
		Timer.stopTimingEvent("File written");
	}
	public static void main(String... args)
	{
		Console.args = args;
	}
}