package utilities;

import java.awt.*;
import java.awt.color.ColorSpace;

public class AdvancedColor extends Color
{
	public AdvancedColor(int r, int g, int b)
	{
		super(r, g, b);
	}

	public AdvancedColor(int r, int g, int b, int a)
	{
		super(r, g, b, a);
	}

	public AdvancedColor(Color a)
	{
		super(a.getRGB());
	}

	public AdvancedColor(int rgb)
	{
		super(rgb);
	}

	public AdvancedColor(int rgba, boolean hasalpha)
	{
		super(rgba, hasalpha);
	}

	public AdvancedColor(float r, float g, float b)
	{
		super(r, g, b);
	}

	public AdvancedColor(float r, float g, float b, float a)
	{
		super(r, g, b, a);
	}

	public AdvancedColor(ColorSpace cspace, float[] components, float alpha)
	{
		super(cspace, components, alpha);
	}

	public boolean isApproximately(Color comparator)
	{
		return isApproximately(comparator, 15);
	}

	public boolean isApproximately(Color comparator, int offset)
	{
		return
			this.getRed() >= comparator.getRed() - offset && this.getRed() <= comparator.getRed() + offset &&
			this.getGreen() >= comparator.getGreen() - offset && this.getGreen() <= comparator.getGreen() + offset &&
			this.getBlue() >= comparator.getBlue() - offset && this.getBlue() <= comparator.getBlue() + offset;
	}
}
