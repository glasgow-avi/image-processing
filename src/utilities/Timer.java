package utilities;

public class Timer
{
	private static double before, after;

	public static void startTimingEvent()
	{
		before = System.currentTimeMillis();
	}

	public static void stopTimingEvent(String eventName)
	{
		after = System.currentTimeMillis();
		System.out.println(eventName + " in " + (after - before) / 1000);
	}
}
