package gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Console extends JFrame
{
	private static JTextField inputField, outputField;
	private static JButton chooseInputFileButton, chooseOutputFileButton;
	private static JFileChooser fileChooser;

	private static final int width = 400, height = 25 * 9;

	JButton processButton;
	public static String[] args;

	public Console()
	{
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new FlowLayout());
		setTitle("Image Processor");
		setResizable(false);

		JLabel inputLabel = new JLabel("Input:");
		inputLabel.setPreferredSize(new Dimension(width, 25));
		inputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(inputLabel);

		inputField = new JTextField();
		inputField.setPreferredSize(new Dimension(width / 4 * 3, 25));
		add(inputField);

		chooseInputFileButton = new JButton("Open");
		add(chooseInputFileButton);
		chooseInputFileButton.addActionListener(new FileChooser(this, inputField));

		JLabel outputLabel = new JLabel("Output:");
		outputLabel.setPreferredSize(new Dimension(width, 25));
		outputLabel.setHorizontalAlignment(SwingConstants.CENTER);
		add(outputLabel);

		outputField = new JTextField();
		outputField.setPreferredSize(new Dimension(width / 4 * 3, 25));
		add(outputField);

		chooseOutputFileButton = new JButton("Open");
		add(chooseOutputFileButton);
		chooseOutputFileButton.addActionListener(new FileChooser(this, outputField));

		JLabel blankLabel = new JLabel();
		blankLabel.setPreferredSize(new Dimension(width, 25));
		add(blankLabel);

		processButton = new JButton("Process");
		add(processButton);
		processButton.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				processButton.setText("Processing...");
				processButton.setEnabled(false);

				args[0] = inputField.getText();
				args[1] = outputField.getText();
				new Thread()
				{
					@Override
					public void run()
					{
						app.ImageProcessor.process(args);
						processButton.setText("Process");
						processButton.setEnabled(true);
					}
				}.start();
			}
		});

		setSize(width, height);
	}

	private class FileChooser extends JFileChooser implements ActionListener
	{
		private Console parent;
		private JTextField textField;

		public FileChooser(Console parent, JTextField textField)
		{
			super();
			this.parent = parent;
			this.textField = textField;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			showDialog(parent, "Open");
			textField.setText(getSelectedFile().getPath());
		}
	}
}
