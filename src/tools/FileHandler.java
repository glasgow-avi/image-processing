package tools;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FileHandler
{
	private static File imageFile;
	private static String defaultFormat = "png";

	public static BufferedImage load(String fileName)
	{
		imageFile = new File(fileName);

		BufferedImage image = null;
		try
		{
			image = ImageIO.read(imageFile);
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return image;
	}

	public static void write(BufferedImage image, String pathName, String format)
	{
		try
		{
			ImageIO.write(image, format, new File(pathName));
		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void write(BufferedImage image, String pathName)
	{
		write(image, pathName, defaultFormat);
	}
}
