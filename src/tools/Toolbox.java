package tools;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Scanner;

public class Toolbox
{
//	private static class PixelTraverser extends Thread
//	{
//		private static class Synchronizer{};
//		private static Synchronizer s = new Synchronizer();
//
//		private Color[][] pixels;
//		private BufferedImage image;
//		private int rowNumber;
//
//		public static int rowsTraversed = 0;
//
//		public PixelTraverser(int rowNumber, Color[][] pixels, BufferedImage image)
//		{
//			this.pixels = pixels;
//			this.image = image;
//			this.rowNumber = rowNumber;
//		}
//
//		@Override
//		public void run()
//		{
//			synchronized (s)
//			{
//				for (int x = 0; x < image.getWidth(); x++)
//					pixels[x][rowNumber] = new Color(image.getRGB(x, rowNumber));
//				rowsTraversed++;
//				if(rowsTraversed == 2448)
//					System.out.println("Finished");
//			}
//		}
//	}

	public static BufferedImage grayScale(BufferedImage originalImage)
	{
		BufferedImage image = newBufferedImage(originalImage);

		for(int y = 0; y < originalImage.getHeight(); y++)
			for(int x = 0; x < originalImage.getWidth(); x++)
			{
				Color c = new Color(originalImage.getRGB(x, y));
				int grayColor = (c.getRed() + c.getBlue() + c.getGreen()) / 3;
				Color gray = new Color(grayColor, grayColor, grayColor, 255);
				image.setRGB(x, y, gray.getRGB());
			}

		return image;
	}

	public static Color[][] getPixels(BufferedImage image)
	{
		Color[][] pixels = new Color[image.getWidth()][image.getHeight()];

		for(int y = 0; y < image.getHeight(); y++)
			for(int x = 0; x < image.getWidth(); x++)
				pixels[x][y] = new Color(image.getRGB(x, y));
//		PixelTraverser[] threads = new PixelTraverser[image.getHeight()];
//
//		for(int y = 0; y < image.getHeight(); y++)
//		{
//			threads[y] = new PixelTraverser(y, pixels, image);
//			threads[y].start();
//		}
//
//		while(PixelTraverser.rowsTraversed < image.getHeight())
//		{
//			try
//			{
//				Thread.sleep(100);
//			} catch (InterruptedException e)
//			{
//				e.printStackTrace();
//			}
//		}
//		PixelTraverser.rowsTraversed = 0;
		return pixels;
	}

	public static BufferedImage putPixels(Color[][] pixels)
	{
		BufferedImage image = newBufferedImage(pixels.length, pixels[0].length);

		for(int y = 0; y < image.getHeight(); y++)
			for(int x = 0; x < image.getWidth(); x++)
				image.setRGB(x, y, pixels[x][y].getRGB());

		return image;
	}

	private static BufferedImage newBufferedImage(int width, int height)
	{
		return new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
	}

	private static BufferedImage newBufferedImage(BufferedImage originalImage)
	{
		return new BufferedImage(originalImage.getWidth(), originalImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
	}
}